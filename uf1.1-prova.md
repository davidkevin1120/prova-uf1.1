# Prova M02 - UF1.1 - 05/11/2018
IES ESCOLA DEL TREBALL BARCELONA
COGNOMS:______________________________________________________________________________________________________________ _____________________________________NOM:_____________________________________________________________________-_______
CURS:_______________________________________________________________________________________________________________

## Conversions de base [3 PUNTS]

1. Converteix els següents codis a les altres bases:

| DECIMAL | BINARI          | HEXADECIMAL | OCTAL |
| ------- | --------------- | ----------- | ----- |
| 4       |                 |             |       |
|         | 1 0 1 0 1 0 1 0 |             |       |
|         |                 | AB          |       |
|         |                 |             | 54    |
| 127     |                 |             |       |
|         | 1 0 0 0 0 0 0 0 |             |       |
|         |                 | 1           |       |
|         |                 |             | 6     |
| 64      |                 |             |       |
|         | 1 0 0 1 1 1 1 1 |             |       |
|         |                 | c4          |       |
|         |                 |             | 78    |


## Conceptes de Sistemes Operatius [4 PUNTS]

1. Quina part del sistema operatiu és l'encarregada de comunicar-se amb els dispositius mitjançant els controladors? Com s'ho farà el sistema operatiu per a gestionar per exemple dades que vénen de dos aplicatius cap a Internet i han de sortir per la mateixa interfície?

Resposta, part del sistema encarregada de comunicar amb els dispositius:

Resposta, gestió de dades per una interfície:

2. Quina diferència hi ha entre la memòria 'cache' i la memòria 'swap'?
$ free -m
              total        used        free      shared  buff/cache   available
Mem:           3746        1337         822         263        1587        1819
Swap:          3875           0        3875

Resposta:

3. En quin fitxer podem trobar la informació de memòria al sistema operatiu Linux? Copieu i enganxeu l'execució i resultat de la comanda en CLI.

Resposta:

4. Quina és la funció del procés del sistema operatiu Linux 'oom_killer'?

Resposta:

5. En quin fitxer podem veure la puntuació (score) del procés l'oom_killer per a un procés qualsevol del sistema operatiu Linux?  Copieu i enganxeu l'execució i resultat de la comanda en CLI.

Resposta:

6. On podem trobar al sistema operatiu Windows el nom del controlador (driver) que està utilitzant per a comunicar-se amb la targeta gràfica? Indiqueu els passos per a mostrar-lo i també el nom del controlador que s'està fent servir.

Resposta (passos per a arribar al controlador:

Resposta (controlador que s'està utilitzant):

7. Què permet fer el servei 'tuned' als sistemes operatius CentOS/Fedora? Indiqueu també com instal·lar-lo i configurar-lo.

Resposta, descripció servei:

Resposta, instal·lació i configuració del servei:

8. Explica la diferència entre bases de dades relacionals i documentals:

Resposta:


## Comandes [3 PUNTS]

1. Quina comanda ens permet veure els dispositius d'emmagatzematge en aquest format?
NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sr0              11:0    1   5,9G  0 rom  /run/media/darta/NOCTURNA_SCN
sda               8:0    0 232,9G  0 disk 
├─sda2            8:2    0 231,9G  0 part 
│ ├─fedora-swap 253:1    0   3,8G  0 lvm  [SWAP]
│ └─fedora-root 253:0    0 228,1G  0 lvm  /
└─sda1            8:1    0     1G  0 part /boot

Resposta:

2. Quina comanda ens permet veure el maquinari de bus detectat pel sistema operatiu en aquest format?
00:00.0 Host bridge: Intel Corporation Core Processor DRAM Controller (rev 02)
00:02.0 VGA compatible controller: Intel Corporation Core Processor Integrated Graphics Controller (rev 02)
00:16.0 Communication controller: Intel Corporation 5 Series/3400 Series Chipset HECI Controller (rev 06)
00:16.3 Serial controller: Intel Corporation 5 Series/3400 Series Chipset KT Controller (rev 06)
00:19.0 Ethernet controller: Intel Corporation 82577LM Gigabit Network Connection (rev 05)
00:1a.0 USB controller: Intel Corporation 5 Series/3400 Series Chipset USB2 Enhanced Host Co...

Resposta:

3. Quina comanda ens permet canviar els permisos del fitxer mostrat per tal que només l'usuari pugui executar-lo?
3826509     4 -rw-r--r--.  1 darta darta      874  2 oct 16:19 install.sh

Resposta:

4. Quins permisos tindrà el grup d'usuaris sobre un fitxer al qual apliquem els permisos de fitxer 654?

Resposta:

5. Com podem canviar el propietari del fitxer install.sh anterior per tal que ara sigui l'usuari 'pepito'?

Resposta:

6. Com podem copiar un fitxer que es troba a la carpeta /usr/local i que s'anomena test.md a la carpeta /usr si ens trobem a la carpeta /usr/local? Indiqueu l'ordre per a fer-ho utilitzant directoris absoluts i utilitzant directoris relatius

Resposta absoluts:                          

Resposta relatius:



